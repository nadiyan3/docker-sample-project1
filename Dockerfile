FROM openjdk:17
ADD target/first-docker.jar first-docker.jar
ENTRYPOINT [ "java","-jar","first-docker.jar" ]